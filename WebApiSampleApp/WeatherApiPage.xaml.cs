﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using WeatherApiSampleApp.Models;
using Xamarin.Forms;

namespace WebApiSampleApp
{
    public partial class WeatherApiPage : ContentPage
    {
        public WeatherApiPage()
        {
            InitializeComponent();
        }


        async void GetWeather_Clicked(object sender, System.EventArgs e)
        {
            HttpClient client = new HttpClient();

            var uri = new Uri(
                string.Format(
                    $"http://api.openweathermap.org/data/2.5/weather?q=Oceanside&units=imperial&APPID=" +
                    $"{Keys.WeatherKey}"));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);
            WeatherItem weatherApiData = null;
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                weatherApiData = WeatherItem.FromJson(content);
                WeatherData.Text = $"Current temp is {weatherApiData.Main.Temp}";
            }
        }
    }
}

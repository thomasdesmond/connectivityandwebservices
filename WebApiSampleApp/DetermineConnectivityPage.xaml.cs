﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace WebApiSampleApp
{
    public partial class DetermineConnectivityPage : ContentPage
    {
        public DetermineConnectivityPage()
        {
            InitializeComponent();
        }

        void Handle_CheckNetworkStatus(object sender, System.EventArgs e)
        {

            if (CrossConnectivity.Current.IsConnected == true)
            {
                ConnectionStatusLabel.Text = "Connected to Internet via " +
                    CrossConnectivity.Current.ConnectionTypes.First();
            }
            else
            {
                ConnectionStatusLabel.Text = "Not Connected to Internet";
            }
        }


    }
}

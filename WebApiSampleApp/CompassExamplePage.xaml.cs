﻿using System;
using System.Collections.Generic;
using Plugin.Compass;
using Xamarin.Forms;

namespace WebApiSampleApp
{
    public partial class CompassExamplePage : ContentPage
    {
        public CompassExamplePage()
        {
            InitializeComponent();

            CrossCompass.Current.Start();


            CrossCompass.Current.CompassChanged += (s, e) =>
            {
                CompassHeadingLabel.Text = $"Current Heading {e.Heading}";
            };
        }


    }
}

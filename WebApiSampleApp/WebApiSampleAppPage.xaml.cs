﻿using Plugin.Settings;
using Xamarin.Forms;

namespace WebApiSampleApp
{
    public partial class WebApiSampleAppPage : ContentPage
    {
        
        public WebApiSampleAppPage()
        {
            InitializeComponent();
            if (CrossSettings.Current.Contains("BackgroundColor"))
            {
                var savedBackgroundColor = CrossSettings.Current.GetValueOrDefault("BackgroundColor", "FFFFFF");
                BackgroundColor = Color.FromHex(savedBackgroundColor);
            }
        }

        void Handle_NavigateToDetermineConnectivityPage(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new DetermineConnectivityPage());
        }

        void Handle_NavigateToWeatherPage(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new WeatherApiPage());
        }

        void Handle_NavigateToCompassPage(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new CompassExamplePage());
        }

        void Handle_ChangeBackgroundColorSetting(object sender, System.EventArgs e)
        {
            var buttonPressed = (Button)sender;
            if (buttonPressed.Text.Contains("White"))
            {
                BackgroundColor = Color.White;
                CrossSettings.Current.AddOrUpdateValue("BackgroundColor", "FFFFFF"); 
            }
            else if (buttonPressed.Text.Contains("Green"))
            {
                BackgroundColor = Color.Green; 
                CrossSettings.Current.AddOrUpdateValue("BackgroundColor", "008000");
            }
            else if (buttonPressed.Text.Contains("Blue"))
            {
                BackgroundColor = Color.Blue;
                CrossSettings.Current.AddOrUpdateValue("BackgroundColor", "0000ff");
            }
        }
    }
}
